# Geek Lite Board

> GEEKROS Team (https://www.geekros.com)

#### 项目说明

> Geek Lite Board机器人主控开发板配套开源程序，开发板拥有非常丰富的外设接口，可覆盖大部分的机器人应用场景。

#### 技术架构

> 开源程序基于STM32标准库、OpenHarmony LiteOS-M内核开发构建，通过内置的USB串口通讯协议封装了相应的Python SDK，对外提供除了C语言以外的Python、图形化编程方式。

### 目录结构

```
/hardware
├── app                  # app模块，用户自定义程序
│   ├── src              # app模块，用户程序源文件目录
│      ├── app.c         # app模块，用户程序，用户开发入口文件
├── liteos_m             # OpenHarmony LiteOS-M精简版内核
├── main                 # main模块，启动程序、外设初始化、任务管理
│   ├── src              # main模块，源文件目录
│      ├── main.c        # main模块，启动程序
│      ├── system_task.c # main模块，系统任务程序
│      ├── serial_task.c # main模块，串口通讯任务程序
│      ├── user_task.c   # main模块，用户任务程序
├── modular              # modular模块，外设API函数库
│   ├── buzzer.c         # modular模块，蜂鸣器API函数库
│   ├── can.c            # modular模块，CAN总线API函数库
│   ...
├── robot                # robot模块，机器人应用源码
│   ├── base
│      ├── src           # robot模块，机器人入口目录
│         ├── robot.c    # robot模块，机器人入口程序
│   ├── lite_dog         # robot模块，机器人程序源文件目录
│      ├── src           # robot模块，机器人入口程序
│         ├── dog_task.c # robot模块，Lite Dog机器狗源码
│         ├── dog_app.c  # robot模块，Lite Dog机器狗源码
│      ...
├── utils                # utils模块，常用中间件
├── core                 # STM32标准库CORE，一般不用更改变动
├── driver               # STM32标准库DRIVER，一般不用更改变动
├── usb                  # STM32标准库USB，一般不用更改变动
/software
├── framework            # 基于串口通讯协议封装的Python SDK
│   ├── board            # 开发板Python API函数库
│   ├── common           # 常用Python API函数库
├── app.py               # 用户程序，用户开发入口文件
```

#### 适配的开发工具

##### Geek Studio【推荐使用】

> Geek Studio是GEEKROS团队自主研发的一款桌面级机器人开发工具，内置了丰富了机器人技术生态，可快速进行机器人的应用开发。
>[前往官网下载](https://www.geekros.com)

##### 其他

> 开源程序编译方式基于GCC、Make、ST-Flash构建，可以适配不同的三方IDE工具，如VSCODE、CLion等，可以参考 [集成开发环境](https://gitee.com/geekros/Geek_Lite_Build) 进行部署。

#### 原生开发方式

> 原生开发支持C语言、Python语言编程，开发前请将开发板USB、ST LINK连接到电脑。

##### Python语言编程

> Python主程序位于【software】目录下的【app.py】文件，使用Python编程时需遵循下面程序模板，可以参考 [开发文档](https://wiki.geekros.com) 的指引调用具体Python API函数。

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import framework as geek_python_sdk

def sdk_start(geek):
    # 在此处编写初始化业务逻辑，此处程序仅运行一次
    while True:
        # 在此处编写主业务逻辑，此处程序循环运行

if __name__ == '__main__':
    sdk = geek_python_sdk.Init()
    sdk_start(sdk)
```

> 完成Python程序编写后可通过下面的命令脚本运行。

```shell script
geek-python ./software/app.py [开发板串口端口]
```

##### C语言编程

> C语言主程序位于【hardware】【src】目录下的【app.c】文件，使用C语言编程时需遵循下面程序模板，可以参考 [开发文档](https://wiki.geekros.com) 的指引调用具体API函数。

```c
/**
  ******************************************************************************
  * @file    app.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "app.h"

/*******************************************************************
  * @ 函数名  ： Setup
  * @ 功能说明： 初始化函数，内部程序仅运行一次
  * @ 参数    ： 无  
  * @ 返回值  ： 无
  *************************************************************/
void Setup()
{
	
}

/*******************************************************************
  * @ 函数名  ： Loop
  * @ 功能说明： 主程序函数，内部程序循环运行
  * @ 参数    ： 无  
  * @ 返回值  ： 无
  *************************************************************/
void Loop()
{
	
}
```

> 完成C程序编写后可通过下面的命令脚本进行编译烧录至开发板。

```shell script
geek-make -j -C ./hardware/
geek-flash --reset write ./hardware/build/firmware.bin 0x8000000
```

### 特别鸣谢

[OpenHarmony](https://gitee.com/openharmony)

[kernel_liteos_m](https://gitee.com/openharmony/kernel_liteos_m)

[build_lite](https://gitee.com/openharmony/build_lite)

[稚晖君](https://space.bilibili.com/20259914)

#### 开源协议

> 基于MIT开源协议进行开源，你可以使用在任何地方，但需保留相关版权信息。
