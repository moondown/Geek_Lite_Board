######################################
# LiteOS M
######################################

# Common
C_SOURCES     += $(wildcard liteos_m/kernel/src/*.c) \
                 $(wildcard liteos_m/kernel/src/mm/*.c) \
                 $(wildcard liteos_m/kal/cmsis/*.c) \
                 $(wildcard liteos_m/utils/*.c) \
                 $(wildcard liteos_m/components/power/*.c) \
                 $(wildcard liteos_m/third_party/bounds_checking_function/src/*.c)
                 
C_INCLUDES    += -Iliteos_m/kernel/include \
				 -Iliteos_m/kal/cmsis \
				 -Iliteos_m/utils \
                 -Iliteos_m/components/power \
                 -Iliteos_m/third_party/bounds_checking_function/include

# Related to arch
ASM_SOURCES   += $(wildcard liteos_m/kernel/arch/arm/cortex-m4/gcc/*.s)

ASMS_SOURCES  += $(wildcard liteos_m/kernel/arch/arm/cortex-m4/gcc/*.S)

C_SOURCES     += $(wildcard liteos_m/kernel/arch/arm/cortex-m4/gcc/*.c)

C_INCLUDES    += -I. \
				 -Iliteos_m/kernel/arch/include \
				 -Iliteos_m/kernel/arch/arm/cortex-m4/gcc

CFLAGS        += -nostdinc -nostdlib

ASFLAGS       +=

# list of ASM .S program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASMS_SOURCES:.S=.o)))
	vpath %.S $(sort $(dir $(ASMS_SOURCES)))

$(BUILD_DIR)/%.o: %.S Makefile | $(BUILD_DIR)
	$(CC) -c $(CFLAGS) $(ASFLAGS) $< -o $@
