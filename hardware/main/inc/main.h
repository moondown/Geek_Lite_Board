#ifndef MAIN
#define MAIN

#include "los_tick.h"
#include "los_task.h"

#include "utils.h"
#include "modular.h"
#include "system_task.h"
#include "serial_task.h"
#include "user_task.h"
#include "robot.h"
#include "servo.h"

static UINT32 Task_Init(void);

static UINT32 Start_System_Task(void);

static UINT32 Start_Serial_Task(void);

static UINT32 Start_User_Task(void);

static UINT32 Start_Robot_Task(void);

#endif
