#ifndef SERIAL_TASK
#define SERIAL_TASK

#include "utils.h"
#include "modular.h"
#include "robot.h"

typedef struct{	
  char* channel;
	char* state;
}Serial_Led_Struct;

typedef struct{
	int channel;
	int id;
	char* state;
}Serial_Power_Struct;

typedef struct{
	int channel;
	int motor_id;
}Serial_Can_Struct;

typedef struct{
	float voltage;
}Serial_Adc_Struct;

typedef struct{
	int channel;
	char* mode;
	char* state;
}Serial_Io_Struct;

typedef struct{
	int channel;
	double rate;
	int width;
}Serial_Pwm_Struct;

typedef struct{
	int rate;
}Serial_Hmi_Struct;

typedef struct{
	int rate;
}Serial_Wifi_Struct;

typedef struct{
	int id;
    int baud_rate;
    int servo_type;
}Serial_Servo_Struct;

typedef struct{
	int rate;
	int s1;
	int s2;
	float ch0;
	float ch1;
	float ch2;
	float ch3;
}Serial_Rocker_Struct;

typedef struct{
	char* file_name;
	char* content;
}Serial_Card_Struct;

typedef struct{
	int robot_type;
}Serial_Robot_Struct;

typedef struct{
	char* type;
	Serial_Led_Struct led;
	Serial_Power_Struct power;
	Serial_Can_Struct can;
	Serial_Adc_Struct adc;
	Serial_Io_Struct io;
	Serial_Pwm_Struct pwm;
	Serial_Hmi_Struct hmi;
	Serial_Wifi_Struct wifi;
	Serial_Servo_Struct servo;
	Serial_Card_Struct card;
	Serial_Rocker_Struct rocker_control;
	Serial_Robot_Struct robot;
}Serial_Data_Struct;

void Serial_Task(void);

void Serial_Json_Handle(char *json);

#endif
