/**
  ******************************************************************************
  * @file    main.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "main.h"

/**************************************************************
  * @ 函数名  ： main
  * @ 功能说明： 程序启动入口，主要初始化一些开发板的模块以及任务
  * @ 参数    ： 无  
  * @ 返回值  ： 无
  *************************************************************/
int main(void)
{
	
	// 定义任务创建的返回值，默认为成功
	UINT32 LiteOS_Status;

    // 时钟初始化
    Delay_Init(LOSCFG_BASE_CORE_TICK_PER_SECOND);

    // USB虚拟串口初始化
    Usb_Init();

    // CPU模块初始化
    Cpu_Init();

    // LED模块初始化
    Led_Init();

    // CAN模块初始化
    Can_Init();

    // POWER模块初始化
    Power_Init();

    // KEY模块初始化
    Key_Init();

    // PWM模块初始化
    Pwm_Init();

    // ADC模块初始化
    Adc_Init();

    // MPU模块初始化
    Mpu_Init();

	// LiteOS内核初始化
	LiteOS_Status = LOS_KernelInit();
	if (LiteOS_Status != LOS_OK) {
        return LOS_NOK;
    }

    // 任务初始化
    LiteOS_Status = Task_Init();
    if (LiteOS_Status != LOS_OK) {
        return LOS_NOK;
    }

    // 开启LiteOS任务调度
    LOS_Start();

	// 正常情况不会执行到这里
	while(1);
}

/**************************************************************
  * @ 函数名  ： Task_Init
  * @ 功能说明： 任务初始化，为了方便管理，所有的任务创建函数都可以放在这个函数里面
  * @ 参数    ： 无  
  * @ 返回值  ： 无
  *************************************************************/
static UINT32 Task_Init(void)
{
	
	// 定义任务创建的返回值，默认为成功
	UINT32 Status;
	
	Status = Start_System_Task();
	if (Status != LOS_OK) {
		// 任务启动失败
		return LOS_NOK;
	}

	Status = Start_Serial_Task();
	if (Status != LOS_OK) {
		// 任务启动失败
		return LOS_NOK;
	}
	
	Status = Start_User_Task();
	if (Status != LOS_OK) {
		// 任务启动失败
		return LOS_NOK;
	}
	
	Status = Start_Robot_Task();
	if (Status != LOS_OK) {
		// 任务启动失败
		return LOS_NOK;
	}

	return Status;
}

/**************************************************************
  * @ 函数名  ： Start_System_Task
  * @ 功能说明： 启动系统任务
  * @ 参数    ： 无
  * @ 返回值  ： 无
  *************************************************************/
UINT32 System_Task_Handle;
static UINT32 Start_System_Task(void)
{
	
	// 定义任务创建的返回值，默认为成功
	UINT32 Task_Status;
	
	TSK_INIT_PARAM_S task_init_param;
	
	// 任务优先级，数值越小，优先级越高
	task_init_param.usTaskPrio = 3;
	
	// 任务名称
	task_init_param.pcName = "System_Task";
	
	// 任务函数入口
	task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)System_Task;
	
	// 任务堆栈大小
	task_init_param.uwStackSize = 1024;
	
	// 创建任务
	Task_Status = LOS_TaskCreate(&System_Task_Handle, &task_init_param);
	
	return Task_Status;
}

/**************************************************************
  * @ 函数名  ： Start_Serial_Task
  * @ 功能说明： 启动串口任务
  * @ 参数    ： 无
  * @ 返回值  ： 无
  *************************************************************/
UINT32 Serial_Task_Handle;
static UINT32 Start_Serial_Task(void)
{
	
	// 定义任务创建的返回值，默认为成功
	UINT32 Task_Status;
	
	TSK_INIT_PARAM_S task_init_param;
	
	// 任务优先级，数值越小，优先级越高
	task_init_param.usTaskPrio = 4;
	
	// 任务名称
	task_init_param.pcName = "Serial_Task";
	
	// 任务函数入口
	task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)Serial_Task;
	
	// 任务堆栈大小
	task_init_param.uwStackSize = 1024;
	
	// 创建任务
	Task_Status = LOS_TaskCreate(&Serial_Task_Handle, &task_init_param);
	
	return Task_Status;
}

/**************************************************************
  * @ 函数名  ： Start_User_Task
  * @ 功能说明： 启动用户任务
  * @ 参数    ： 无
  * @ 返回值  ： 无
  *************************************************************/
UINT32 User_Task_Handle;
static UINT32 Start_User_Task(void)
{
	
	// 定义任务创建的返回值，默认为成功
	UINT32 Task_Status;
	
	TSK_INIT_PARAM_S task_init_param;
	
	// 任务优先级，数值越小，优先级越高
	task_init_param.usTaskPrio = 5;
	
	// 任务名称
	task_init_param.pcName = "User_Task";
	
	// 任务函数入口
	task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)User_Task;
	
	// 任务堆栈大小
	task_init_param.uwStackSize = 2048;
	
	// 创建任务
	Task_Status = LOS_TaskCreate(&User_Task_Handle, &task_init_param);
	
	return Task_Status;
}

/**************************************************************
  * @ 函数名  ： Start_Robot_Task
  * @ 功能说明： 机器人任务
  * @ 参数    ： 无
  * @ 返回值  ： 无
  *************************************************************/
UINT32 Robot_Task_Handle;
static UINT32 Start_Robot_Task(void)
{
	
	// 定义任务创建的返回值，默认为成功
	UINT32 Task_Status;
	
	TSK_INIT_PARAM_S task_init_param;
	
	// 任务优先级，数值越小，优先级越高
	task_init_param.usTaskPrio = 6;
	
	// 任务名称
	task_init_param.pcName = "Robot_Task";
	
	// 任务函数入口
	task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)Robot_Task;
	
	// 任务堆栈大小
	task_init_param.uwStackSize = 2048;
	
	// 创建任务
	Task_Status = LOS_TaskCreate(&Robot_Task_Handle, &task_init_param);
	
	return Task_Status;
}

