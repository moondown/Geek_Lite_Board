/**
  ******************************************************************************
  * @file    serial_task.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "serial_task.h"
#include "dog_order.h"
#include "dog_app.h"
#include <stdlib.h>

static Serial_Data_Struct Read_Data;

uint32_t Serial_Task_Buffer_Len = 512;

uint8_t* Serial_Task_Buffer;


/******************************************************************
  * @ 函数名  ： Serial_Task
  * @ 功能说明： USB虚拟串口任务函数
  * @ 参数    ： NULL 
  * @ 返回值  ： NULL
  *****************************************************************/
void Serial_Task(void)
{
	
	while(1)
	{
		Serial_Task_Buffer = (uint8_t*)malloc(512);

		// 拿到USB虚拟串口的实时数据
		Usb_Read_Data((uint8_t*)Serial_Task_Buffer, Serial_Task_Buffer_Len);

		// 进行JSON通讯协议解析与处理
		Serial_Json_Handle((char*)Serial_Task_Buffer);

		free(Serial_Task_Buffer);
		Serial_Task_Buffer = NULL;
		
		osDelay(10);
	}
}

/******************************************************************
  * @ 函数名  ： Serial_Json_Handle
  * @ 功能说明： JSON通讯协议解析与处理函数
  * @ 参数    ： NULL 
  * @ 返回值  ： NULL
  *****************************************************************/
void Serial_Json_Handle(char *json)
{
	// 解析USB虚拟串口数据
	cJSON	*Serial_Data;
	Serial_Data = cJSON_Parse(json);
	if(Serial_Data){
		
		Read_Data.type = cJSON_GetObjectItem(Serial_Data, "type")->valuestring;
		
		// LED模块
		if(memcmp(Read_Data.type, "led", 3) == 0)
		{
			Read_Data.led.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valuestring;
			Read_Data.led.state = cJSON_GetObjectItem(Serial_Data, "state")->valuestring;
			Led_Usb_Callback(Read_Data.type, Read_Data.led.channel, Read_Data.led.state);
		}
		
		// CAN模块
		if(memcmp(Read_Data.type, "can", 3) == 0)
		{
			Read_Data.can.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valueint;
			Read_Data.can.motor_id = cJSON_GetObjectItem(Serial_Data, "motor_id")->valueint;
			Can_Usb_Callback(Read_Data.type, Read_Data.can.channel, Read_Data.can.motor_id);
		}
		
		// POWER模块
		if(memcmp(Read_Data.type, "power", 5) == 0)
		{
			Read_Data.power.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valueint;
			Read_Data.power.id = cJSON_GetObjectItem(Serial_Data, "id")->valueint;
			Read_Data.power.state = cJSON_GetObjectItem(Serial_Data, "state")->valuestring;
			Power_Usb_Callback(Read_Data.type, Read_Data.power.channel, Read_Data.power.id, Read_Data.power.state);
		}
		
		// KEY模块
		if(memcmp(Read_Data.type, "key", 3) == 0)
		{
			Key_Usb_Callback(Read_Data.type);
		}
		
		// ADC模块
		if(memcmp(Read_Data.type, "adc", 3) == 0)
		{
			Read_Data.adc.voltage = cJSON_GetObjectItem(Serial_Data, "voltage")->valuedouble;
			Adc_Usb_Callback(Read_Data.type, Read_Data.adc.voltage);
		}
		
		// IO模块
		if(memcmp(Read_Data.type, "io", 2) == 0)
		{
			Read_Data.io.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valueint;
			Read_Data.io.mode = cJSON_GetObjectItem(Serial_Data, "mode")->valuestring;
			Read_Data.io.state = cJSON_GetObjectItem(Serial_Data, "state")->valuestring;
			Io_Usb_Callback(Read_Data.type, Read_Data.io.channel, Read_Data.io.mode, Read_Data.io.state);
		}
		
		// PWM模块
		if(memcmp(Read_Data.type, "pwm", 3) == 0)
		{
			Read_Data.pwm.channel = cJSON_GetObjectItem(Serial_Data, "channel")->valueint;
			Read_Data.pwm.rate = cJSON_GetObjectItem(Serial_Data, "rate")->valuedouble;
			Read_Data.pwm.width = cJSON_GetObjectItem(Serial_Data, "width")->valueint;
			Pwm_Usb_Callback(Read_Data.type, Read_Data.pwm.channel, Read_Data.pwm.rate, Read_Data.pwm.width);
		}
		
		// CPU模块
		if(memcmp(Read_Data.type, "cpu", 3) == 0)
		{
			Cpu_Usb_Callback(Read_Data.type);
		}
		
		// MPU模块
		if(memcmp(Read_Data.type, "mpu", 3) == 0)
		{
			Mpu_Usb_Callback(Read_Data.type);
		}

		// HMI模块
        if(memcmp(Read_Data.type, "hmi", 3) == 0)
        {
            Read_Data.hmi.rate = cJSON_GetObjectItem(Serial_Data, "rate")->valueint;
            Hmi_Usb_Callback(Read_Data.type, Read_Data.hmi.rate);
        }

        // WIFI模块
        if(memcmp(Read_Data.type, "wifi", 4) == 0)
        {
            Read_Data.wifi.rate = cJSON_GetObjectItem(Serial_Data, "rate")->valueint;
            Wifi_Usb_Callback(Read_Data.type, Read_Data.wifi.rate);
        }

        // SERVO模块
        if(memcmp(Read_Data.type, "servo", 5) == 0)
        {
            Read_Data.servo.id = cJSON_GetObjectItem(Serial_Data, "id")->valueint;
            Read_Data.servo.baud_rate = cJSON_GetObjectItem(Serial_Data, "rate")->valueint;
            Read_Data.servo.servo_type = cJSON_GetObjectItem(Serial_Data, "servo_type")->valueint;
            Servo_Usb_Callback(Read_Data.type, Read_Data.servo.id, Read_Data.servo.baud_rate, Read_Data.servo.servo_type);
        }

        // SCARD模块
        if(memcmp(Read_Data.type, "card", 4) == 0)
        {
            Read_Data.card.file_name = cJSON_GetObjectItem(Serial_Data, "file")->valuestring;
            Read_Data.card.content = cJSON_GetObjectItem(Serial_Data, "content")->valuestring;
            Card_Usb_Callback(Read_Data.type, Read_Data.card.file_name, Read_Data.card.content);
        }
		
		// 遥控器模块
		if(memcmp(Read_Data.type, "remote-control", 14) == 0)
		{
			Read_Data.rocker_control.rate = cJSON_GetObjectItem(Serial_Data, "rate")->valueint;
			Read_Data.rocker_control.s1 = cJSON_GetObjectItem(Serial_Data, "s1")->valueint;
			Read_Data.rocker_control.s2 = cJSON_GetObjectItem(Serial_Data, "s2")->valueint;
			Read_Data.rocker_control.ch0 = cJSON_GetObjectItem(Serial_Data, "ch0")->valuedouble;
			Read_Data.rocker_control.ch1 = cJSON_GetObjectItem(Serial_Data, "ch1")->valuedouble;
			Read_Data.rocker_control.ch2 = cJSON_GetObjectItem(Serial_Data, "ch2")->valuedouble;
			Read_Data.rocker_control.ch3 = cJSON_GetObjectItem(Serial_Data, "ch3")->valuedouble;
			Rocker_Write_Data(Read_Data.rocker_control.s1, Read_Data.rocker_control.s2, Read_Data.rocker_control.ch0, Read_Data.rocker_control.ch1, Read_Data.rocker_control.ch2, Read_Data.rocker_control.ch3);
			// Rocker_Usb_Callback(Read_Data.type, Read_Data.rocker_control.rate, Read_Data.rocker_control.s1, Read_Data.rocker_control.s2, Read_Data.rocker_control.ch0, \
			// Read_Data.rocker_control.ch1, Read_Data.rocker_control.ch2, Read_Data.rocker_control.ch3);
		}

		// ROBOT模块
        if(memcmp(Read_Data.type, "robot", 5) == 0)
        {
            Read_Data.robot.robot_type = cJSON_GetObjectItem(Serial_Data, "robot_type")->valueint;
            Robot_Usb_Callback(Read_Data.type, Read_Data.robot.robot_type);
        }

		// Flash模块
        if(memcmp(Read_Data.type, "flash", 5) == 0)
        {
            char* Flash_Data_Addr = cJSON_GetObjectItem(Serial_Data, "Addr")->valuestring;
			char* Flash_Data_Data = (u8*)cJSON_GetObjectItem(Serial_Data, "Data")->valuestring;
			u32 Flash_Data_DLen = cJSON_GetObjectItem(Serial_Data, "DLen")->valueint;
			Flash_Callback(Read_Data.type,Flash_Data_Addr,Flash_Data_Data,Flash_Data_DLen);
        }

		// Pwm电机模块
        if(memcmp(Read_Data.type, "motor", 5) == 0)
        {
            int Motor_Num = cJSON_GetObjectItem(Serial_Data, "Num")->valueint;
			int Motor_Fre = cJSON_GetObjectItem(Serial_Data, "Fre")->valueint;
			int Motor_Spe = cJSON_GetObjectItem(Serial_Data, "Speed")->valueint;
			Pwm_Motor_Speed_Set(Motor_Num,Motor_Fre,Motor_Spe);
        }

		// Dog_Test模块
        if(memcmp(Read_Data.type, "Dog", 3) == 0)
        {
            char* Dog_Leg = cJSON_GetObjectItem(Serial_Data, "Leg")->valuestring;
			Dog_Test_Usb_Callback(Read_Data.type,Dog_Leg);
        }

		// 狗狗运动模式控制设置
		if(memcmp(Read_Data.type, "lite-dog-control", 15) == 0){
			int cycle = cJSON_GetObjectItem(Serial_Data, "cycle")->valueint;
			char* mode = cJSON_GetObjectItem(Serial_Data, "mode")->valuestring;
			char* control = cJSON_GetObjectItem(Serial_Data, "control")->valuestring;
			Dog_Control_Serial_Callback(cycle, mode, control);
		}

		// 狗狗全向运动模式控制
		if(memcmp(Read_Data.type, "lite-dog-omni", 13) == 0){
			int cycle = cJSON_GetObjectItem(Serial_Data, "cycle")->valueint;
			char* mode = cJSON_GetObjectItem(Serial_Data, "mode")->valuestring;
			float angle = cJSON_GetObjectItem(Serial_Data, "angle")->valuedouble;
			Dog_Omni_Control_Serial_Callback(cycle, mode, angle);
		}

		// 狗狗姿态调整
		if(memcmp(Read_Data.type, "lite-dog-attitude", 16) == 0){
			float XYZ_DATA[3];
			XYZ_DATA[0] = cJSON_GetObjectItem(Serial_Data, "x")->valuedouble;
			XYZ_DATA[1] = cJSON_GetObjectItem(Serial_Data, "y")->valuedouble;
			XYZ_DATA[2] = cJSON_GetObjectItem(Serial_Data, "z")->valuedouble;
			int roll = cJSON_GetObjectItem(Serial_Data, "roll")->valueint;
			int pitch = cJSON_GetObjectItem(Serial_Data, "pitch")->valueint;
			int gravity = cJSON_GetObjectItem(Serial_Data, "gravity")->valueint;
			Dog_Attitude_Serial_Callback(XYZ_DATA,roll,pitch,gravity);
		}

		// 狗狗校准
		if(memcmp(Read_Data.type, "lite-dog-calibration", 19) == 0){
			char* leg = cJSON_GetObjectItem(Serial_Data, "leg")->valuestring;
			char* State = cJSON_GetObjectItem(Serial_Data, "State")->valuestring;
			Dog_Celebration_Serial_Callback(leg,State);
		}

		// 狗狗XY
		if(memcmp(Read_Data.type, "lite-dog-XY", 10) == 0){
			char* leg = cJSON_GetObjectItem(Serial_Data, "leg")->valuestring;
			int x_d = cJSON_GetObjectItem(Serial_Data, "X_Data")->valueint;
			int y_d = cJSON_GetObjectItem(Serial_Data, "Y_Data")->valueint;
			int z_d = cJSON_GetObjectItem(Serial_Data, "Z_Data")->valueint;
			int speed = cJSON_GetObjectItem(Serial_Data, "Speed_Data")->valueint;
			int aspeed = cJSON_GetObjectItem(Serial_Data, "Aspeed_Data")->valueint;
			Dog_XYZ_Set_Serial_Callback(leg,x_d,y_d,z_d,speed,aspeed);
		}

		// 狗狗自动模式
		if(memcmp(Read_Data.type, "lite-dog-action", 14) == 0){
			char* Mode_Set = cJSON_GetObjectItem(Serial_Data, "action")->valuestring;
			Dog_Auto_Mode_Serial_Callback(Mode_Set);
		}

		// 狗狗整体模式切换
		if(memcmp(Read_Data.type, "lite-dog-ModeChange", 18) == 0){
			char* Mode_Set = cJSON_GetObjectItem(Serial_Data, "Mode")->valuestring;
			Dog_Mode_Serial_Callback(Mode_Set);
		}

		
	}
	
	// 需要清空本次收到的数据
	cJSON_Delete(Serial_Data);
	Read_Data.type = NULL;
	memset(json,0,sizeof(json));
}
