#ifndef MODULAR_CARD
#define MODULAR_CARD

#include "usb.h"
#include <stdlib.h>
#include "sd_card.h"
#include "ff_debug.h"
#include "ff.h"

void Card_Create_File(char *file_name, char *content);

void Card_Append_Content(char *file_name, char *content);

void Card_Read_File(char *file_name,char *content);

void Card_Delete_File(char *file_name);

void Card_Usb_Callback(char *type, char *file_name, char *content);

#endif
