#ifndef MODULAR
#define MODULAR

#include "utils.h"
#include "led.h"
#include "usb.h"
#include "buzzer.h"
#include "can.h"
#include "power.h"
#include "key.h"
#include "adc.h"
#include "io.h"
#include "pwm.h"
#include "cpu.h"
#include "mpu.h"
#include "flash.h"
#include "rocker.h"
#include "wifi.h"
#include "servo.h"
#include "hmi.h"
#include "radar.h"
#include "card.h"
#include "pwm_motor.h"

void Usb_State_Task(void);

#endif
