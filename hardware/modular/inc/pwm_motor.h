#ifndef MODULAR_PWM_MOTOR
#define MODULAR_PWM_MOTOR


#include "pwm.h"

void Pwm_Motor_Init(int Motor_Num,int Motor_Init_Frequency,int Motor_Init_Time);
void Pwm_Motor_Speed_Set(int Motor_Num,int Motor_Frequency,int Motor_Speed_Set);

#endif
