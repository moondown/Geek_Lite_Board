/**
  ******************************************************************************
  * @file    card.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "card.h"

/******************************************************************
  * @ 函数名  ： Card_Create_File
  * @ 功能说明： 创建文件
  * @ 参数    ： har *file_name 文件名称
  * @ 参数    ： char *content 文件内容
  * @ 返回值  ： NULL
  *****************************************************************/
void Card_Create_File(char *file_name, char *content)
{
	uint32_t 	i = 0;
 	FATFS   	fs;
	FIL     	file;
	FRESULT		fresult;
	TCHAR		filename[128];

	//注册/注销一个工作区域
	fresult = f_mount(0, &fs);
	//创建/打开一个文件
	sprintf(filename,"0:%s",file_name);
	fresult = f_open(&file, filename, FA_OPEN_ALWAYS | FA_WRITE);	
	//写一个格式化的字符串
	i = f_printf(&file,content);			
	//关闭一个文件
	fresult = f_close(&file);
	f_mount(0, NULL);
}

/******************************************************************
  * @ 函数名  ： Card_Append_Content
  * @ 功能说明： 向已有的文件追加内容
  * @ 参数    ： char *file_name 文件名称
  * @ 参数    ： char *content 文件内容
  * @ 返回值  ： NULL
  *****************************************************************/
void Card_Append_Content(char *file_name, char *content)
{
	volatile int i;
 	FATFS   	fs;
	FIL     	file;
	volatile FRESULT		fresult;
	TCHAR		filename[128];
		
	int byte;
		
	fresult = f_mount(0, &fs);

	sprintf(filename,"0:%s",file_name);
	
	fresult = f_open(&file, filename, FA_OPEN_EXISTING | FA_WRITE);
	
	byte = f_size(&file);

	f_lseek(&file, byte);
	i = f_printf(&file,content);
	
	fresult = f_close(&file);

//注销一个工作区域
	f_mount(0, NULL);
}

/******************************************************************
  * @ 函数名  ： Card_Read_File
  * @ 功能说明： 读取文件内容
  * @ 参数    ： har *file_name 文件名称
  * @ 参数    ： char *content 文件内容保存地址
  * @ 返回值  ： NULL
  *****************************************************************/
void Card_Read_File(char *file_name,char *content)
{
 	FATFS   	fs;
	FIL     	file;
	volatile FRESULT		fresult;
	char		buffstr[1024];
	TCHAR		filename[128];
	int byte;
		
	fresult = f_mount(0, &fs);
	// FR_print_error(fresult);
	//=========================================================================
	//打开一个文件
	sprintf(filename,"0:%s",file_name);
	fresult = f_open(&file, filename, FA_OPEN_EXISTING | FA_READ);
	// FR_print_error(fresult);

	byte = f_size(&file);
	f_lseek(&file, 0);
  if(byte > 512)
    byte = 512;
	fresult = f_read(&file,buffstr,byte,0);
	// FR_print_error(fresult);

	fresult = f_close(&file);
  // FR_print_error(fresult);
  
		for(int i = byte;i <= 512;i++)
		{
			buffstr[i] = (char)0;
		}
		for(int i = 0;i < byte;i++)
		{
			content[i] = buffstr[i];
		}
		
	f_mount(0, NULL);
}

/******************************************************************
  * @ 函数名  ： Card_Delete_File
  * @ 功能说明： 删除文件
  * @ 参数    ： har *file_name 文件名称
  * @ 返回值  ： NULL
  *****************************************************************/
void Card_Delete_File(char *file_name)
{
	volatile FRESULT		fresult;
	FATFS   	fs;
	TCHAR		filename[16];
	fresult = f_mount(0, &fs);
	
	sprintf(filename,"0:%s",file_name);
	fresult = f_unlink(filename);

	f_mount(0, NULL);
}

/******************************************************************
  * @ 函数名  ： Card_Scan
  * @ 功能说明： 列出文件
  * @ 参数    ： char *path 文件名称
  * @ 返回值  ： NULL
  *****************************************************************/
void Card_Scan(char *path)
{
	volatile int error_flag;
	volatile FRESULT		fresult;
	FATFS   	fs;
	fresult = f_mount(0, &fs);
	Usb_Write_Data("Scan files:\n");
	if((path[0] == '0') && (path[1] == ':'))
  {
    fresult = scan_files_usb("0:");
  }
  else
  {
    Usb_Write_Data("path error!!! \r\n");
  }
	f_mount(0, NULL);
}

/******************************************************************
  * @ 函数名  ： Card_Usb_Callback
  * @ 功能说明： 串口回调函数
  * @ 参数    ： NULL
  * @ 返回值  ： NULL
  *****************************************************************/
void Card_Usb_Callback(char *type, char *file_name, char *content)
{
	if(memcmp(type, "card-create-file", 16) == 0)
	{
	    Card_Create_File(file_name,content);
	}
	if(memcmp(type, "card-append-content", 19) == 0)
  {
      Card_Append_Content(file_name, content);
  }
  if(memcmp(type, "card-delete-file", 16) == 0)
  {
      Card_Delete_File(file_name);
  }
  if(memcmp(type, "card-read-file", 14) == 0)
  {
      char* Read_File;
      Read_File = (u8*)malloc(512);
      Card_Read_File(file_name,Read_File);
      Usb_Write_Data("{\"type\":\"card-read-file\",\"file\":\"66.txt\",\"content\":\"%x\"}",Read_File);
      free(Read_File);
  }
  if(memcmp(type, "card-scan", 9) == 0)
  {
      Card_Scan(file_name);
  }
}