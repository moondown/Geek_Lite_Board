/**
  ******************************************************************************
  * @file    key.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "key.h"

/******************************************************************
  * @ 函数名  ： Key_Init
  * @ 功能说明： KEY模块初始化
  * @ 参数    ： NULL 
  * @ 返回值  ： NULL
  *****************************************************************/
void Key_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
}

/******************************************************************
  * @ 函数名  ： Key_Usb_Callback
  * @ 功能说明： 串口回调函数
  * @ 参数    ： char* type 类型 
  * @ 返回值  ： NULL
  *****************************************************************/
void Key_Usb_Callback(char *type){
	if(memcmp(type, "key-state", 9) == 0){
		Usb_Write_Data("{\"type\":\"key-state\",\"state\":%d}\r\n", Key_State);
	}
}
