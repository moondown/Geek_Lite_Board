/**
  ******************************************************************************
  * @file    led.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "led.h"

/******************************************************************
  * @ 函数名  ： Led_Init
  * @ 功能说明： LED模块初始化
  * @ 参数    ： NULL 
  * @ 返回值  ： NULL
  *****************************************************************/
void Led_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(GPIOE,&GPIO_InitStructure);
    
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(GPIOF,&GPIO_InitStructure);
    
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(GPIOH,&GPIO_InitStructure);
    
    GREEN_LED_OFF();
    RED_LED_OFF();
    BLUE_LED_OFF();
}

/******************************************************************
  * @ 函数名  ： Led_All_State
  * @ 功能说明： 控制所有LED状态
  * @ 参数    ： char *state 状态值off、on、switch
  * @ 返回值  ： NULL
  *****************************************************************/
void Led_All_State(char *state)
{
	if(memcmp(state, "off", 3) == 0)
	{
		GREEN_LED_OFF();
		RED_LED_OFF();
		BLUE_LED_OFF();
	}
	if(memcmp(state, "on", 2) == 0)
	{
		GREEN_LED_ON();
		RED_LED_ON();
		BLUE_LED_ON();
	}
	if(memcmp(state, "switch", 6) == 0)
	{
		GREEN_LED_TOGGLE();
		RED_LED_TOGGLE();
		BLUE_LED_TOGGLE();
	}
}

/******************************************************************
  * @ 函数名  ： Led_Status
  * @ 功能说明： 控制某个LED状态
  * @ 参数    ： char *channel 通道green、red、blue
	* @ 参数    ： char *state 状态值off、on、switch
  * @ 返回值  ： NULL
  *****************************************************************/
void Led_State(char *channel, char *state)
{
	if(memcmp(channel, "green", 5) == 0)
	{
		if(memcmp(state, "switch", 6) == 0)
		{
			GREEN_LED_TOGGLE();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			GREEN_LED_OFF();
		}
		if(memcmp(state, "on", 2) == 0)
		{
			GREEN_LED_ON();
		}
	}
	if(memcmp(channel, "red", 3) == 0)
	{
		if(memcmp(state, "switch", 6) == 0)
		{
			RED_LED_TOGGLE();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			RED_LED_OFF();
		}
		if(memcmp(state, "on", 2) == 0)
		{
			RED_LED_ON();
		}
	}
	if(memcmp(channel, "blue", 4) == 0)
	{
		if(memcmp(state, "switch", 6) == 0)
		{
			BLUE_LED_TOGGLE();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			BLUE_LED_OFF();
		}
		if(memcmp(state, "on", 2) == 0)
		{
			BLUE_LED_ON();
		}
	}
}

/******************************************************************
  * @ 函数名  ： Led_Usb_Callback
  * @ 功能说明： 串口回调函数
  * @ 参数    ： char *type 类型
  * @ 参数    ： char *channel 通道green、red、blue
	* @ 参数    ： char *state 状态值off、on、switch
  * @ 返回值  ： NULL
  *****************************************************************/
void Led_Usb_Callback(char *type, char *channel, char *state)
{
	if(memcmp(type, "led-state", 9) == 0)
	{
		if(memcmp(channel, "all", 3) == 0)
		{
			Led_All_State(state);
		}else{
			Led_State(channel, state);
		}
	}
}


void Heart_Led_Set(char *channel,int Time)
{
	static int Time_Count = 0;
	if((Time_Count*Time)%500  == 0)
	{
		if(memcmp(channel, "green", 5) == 0)
		{
			GREEN_LED_TOGGLE();
		}
		if(memcmp(channel, "red", 3) == 0)
		{
			RED_LED_TOGGLE();
		}
		if(memcmp(channel, "blue", 4) == 0)
		{
			BLUE_LED_TOGGLE();
		}
		Time_Count = 0;
	}
	Time_Count++;
}
