/**
  ******************************************************************************
  * @file    power.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "power.h"

/******************************************************************
  * @ 函数名  ： Power_Init
  * @ 功能说明： POWER模块初始化，主要初始化板载5v、24v可控电源接口
  * @ 参数    ： NULL 
  * @ 返回值  ： NULL
  *****************************************************************/
void Power_Init(void)
{
	Power_24v_Init();
	Power_5v_Init();
}

/******************************************************************
  * @ 函数名  ： Power_24v_Init
  * @ 功能说明： 24v可控电源接口初始化，开发板共4组24v可控电源接口
  * @ 参数    ： NULL 
  * @ 返回值  ： NULL
  *****************************************************************/
void Power_24v_Init(void)
{
	GPIO_InitTypeDef gpio;
    
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH,ENABLE);
	
	gpio.GPIO_Pin = GPIO_Pin_2 |  GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOH,&gpio);
	
	// 默认关闭所有24v电源接口
	POWER1_24V_OFF();
	POWER2_24V_OFF();
	POWER3_24V_OFF();
	POWER4_24V_OFF();
}

/******************************************************************
  * @ 函数名  ： Power_5v_Init
  * @ 功能说明： 5v可控电源接口初始化，开发板共2组5v可控电源接口
  * @ 参数    ： NULL 
  * @ 返回值  ： NULL
  *****************************************************************/
void Power_5v_Init(void)
{
	GPIO_InitTypeDef gpio;
    
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB, ENABLE);
	
	gpio.GPIO_Pin = GPIO_Pin_15;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA,&gpio);
	
	gpio.GPIO_Pin = GPIO_Pin_11;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOB,&gpio);
	
	// 默认关闭所有5v电源接口
	POWER_5V_A_OFF();
	POWER_5V_B_OFF();
}

/******************************************************************
  * @ 函数名  ： Power_5V_State
  * @ 功能说明： 设置5v可控电源状态
  * @ 参数    ： int id 通道ID 可选值：0、1、2
  * @ 参数    ： char* state 通道ID：可选值：on、off
  * @ 返回值  ： NULL
  *****************************************************************/
void Power_5V_State(int id, char* state)
{
	if(id == 0)
	{
		if(memcmp(state, "on", 2) == 0)
		{
			POWER_5V_A_ON();
			POWER_5V_B_ON();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			POWER_5V_A_OFF();
			POWER_5V_B_OFF();
		}
	}
	if(id == 1)
	{
		if(memcmp(state, "on", 2) == 0)
		{
			POWER_5V_A_ON();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			POWER_5V_A_OFF();
		}
	}
	if(id == 2)
	{
		if(memcmp(state, "on", 2) == 0)
		{
			POWER_5V_B_ON();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			POWER_5V_B_OFF();
		}
	}
}

/******************************************************************
  * @ 函数名  ： Power_24V_State
  * @ 功能说明： 设置24v可控电源状态
  * @ 参数    ： int id 通道ID 可选值：0、1、2、3、4
  * @ 参数    ： char* state 通道ID：可选值：on、off
  * @ 返回值  ： NULL
  *****************************************************************/
void Power_24V_State(int id, char* state)
{
	if(id == 0)
	{
		if(memcmp(state, "on", 2) == 0)
		{
			POWER1_24V_ON();
			POWER2_24V_ON();
			POWER3_24V_ON();
			POWER4_24V_ON();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			POWER1_24V_OFF();
			POWER2_24V_OFF();
			POWER3_24V_OFF();
			POWER4_24V_OFF();
		}
	}
	if(id == 1)
	{
		if(memcmp(state, "on", 2) == 0)
		{
			POWER1_24V_ON();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			POWER1_24V_OFF();
		}
	}
	if(id == 2)
	{
		if(memcmp(state, "on", 2) == 0)
		{
			POWER2_24V_ON();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			POWER2_24V_OFF();
		}
	}
	if(id == 3)
	{
		if(memcmp(state, "on", 2) == 0)
		{
			POWER3_24V_ON();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			POWER3_24V_OFF();
		}
	}
	if(id == 4)
	{
		if(memcmp(state, "on", 2) == 0)
		{
			POWER4_24V_ON();
		}
		if(memcmp(state, "off", 3) == 0)
		{
			POWER4_24V_OFF();
		}
	}
}

/******************************************************************
  * @ 函数名  ： Power_State
  * @ 功能说明： 设置可控电源状态
  * @ 参数    ： int channel 通道ID 可选值：5、24
  * @ 参数    ： int id 通道ID 可选值：0、1、2、3、4
  * @ 参数    ： char* state 通道ID：可选值：on、off
  * @ 返回值  ： NULL
  *****************************************************************/
void Power_State(int channel, int id, char* state)
{
	if(channel == 24){
		Power_24V_State(id, state);
	}
	if(channel == 5){
		Power_5V_State(id, state);
	}
}

/******************************************************************
  * @ 函数名  ： Power_Usb_Callback
  * @ 功能说明： 串口回调函数
  * @ 参数    ： int channel 通道ID 可选值：5、24
  * @ 参数    ： int id 接口ID 可选值：0、1、2、3、4
  * @ 参数    ： char* state 通道ID：可选值：on、off
  * @ 返回值  ： NULL
  *****************************************************************/
void Power_Usb_Callback(char* type, int channel, int id, char* state)
{
	if(memcmp(type, "power-state", 11) == 0)
	{
		Power_State(channel, id, state);
	}
}
