/**
  ******************************************************************************
  * @file    pwm_motor.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "pwm_motor.h"

/******************************************************************
  * @ 函数名  ： Pwm_Motor_Init
  * @ 功能说明： PWM模块初始化
  * @ 参数    ： int Motor_Num 电机编号
  * @ 参数    ： int Motor_Init_Frequency 电机初始化频率
  * @ 参数    ： int Motor_Init_Time 电机初始化控制行程
  * @ 返回值  ： NULL
  *****************************************************************/
void Pwm_Motor_Init(int Motor_Num,int Motor_Init_Frequency,int Motor_Init_Time)
{
  if((Motor_Num >= 1) && (Motor_Num <= 4))
    TIM4_Config(PWM_RESOLUTION,TIM_PSC_APB1,SERVO_DEFAULT_DUTY,PWM_CounterMode_Down,PWMO_OCMode_PWM1,PWM_OCPolarity_High);
  else if((Motor_Num >= 5) && (Motor_Num <= 8))
    TIM5_Config(PWM_RESOLUTION,TIM_PSC_APB1,SERVO_DEFAULT_DUTY,PWM_CounterMode_Down,PWMO_OCMode_PWM1,PWM_OCPolarity_High);
  else if((Motor_Num >= 9) && (Motor_Num <= 12))
    TIM2_Config(PWM_RESOLUTION,TIM_PSC_APB1,SERVO_DEFAULT_DUTY,PWM_CounterMode_Down,PWMO_OCMode_PWM1,PWM_OCPolarity_High);
  else if((Motor_Num >= 13) && (Motor_Num <= 16))
    TIM8_Config(PWM_RESOLUTION,TIM_PSC_APB1,SERVO_DEFAULT_DUTY,PWM_CounterMode_Down,PWMO_OCMode_PWM1,PWM_OCPolarity_High);
	Pwm_Motion(Motor_Num, Motor_Init_Frequency, Motor_Init_Time);
}

/******************************************************************
  * @ 函数名  ： Pwm_Motor_Speed_Set
  * @ 功能说明： PWM电机模块设置
  * @ 参数    ： int Motor_Num 电机编号
  * @ 参数    ： int Motor_Frequency 电机频率
  * @ 参数    ： int Motor_Time 电机控制行程
  * @ 返回值  ： NULL
  *****************************************************************/
void Pwm_Motor_Speed_Set(int Motor_Num,int Motor_Frequency,int Motor_Speed_Set)
{
  Pwm_Motion(Motor_Num,Motor_Frequency,Motor_Speed_Set);
}
