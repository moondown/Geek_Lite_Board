#ifndef ROBOT
#define ROBOT

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "lite_dog.h"

typedef enum
{
	ROBOT_NONE = 0,
	LITE_DOG = 1, //精简舵机机器狗
	EDU_DOG = 2, //教育电机机器狗
	ATOM_CAR = 3, //原子全能小车
	VECTOR_ARM = 4, //矢量机械臂
	BALANCE_CAR = 5 //双足平衡小车
}Robot_Type_Struct;

typedef struct
{
	Robot_Type_Struct Robot_Type;
}Robot_Struct;

extern Robot_Struct Robot;

void Robot_Type(int robot_type);

void Robot_Task(void);

void Robot_Usb_Callback(char *type, int robot_type);

#endif
