/**
  ******************************************************************************
  * @file    robot.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "robot.h"

Robot_Struct Robot;

/******************************************************************
  * @ 函数名  ： Robot_Type
  * @ 功能说明： 机器人类型设置函数
  * @ 参数    ： int robot_type 机器人类型
  * @ 返回值  ： NULL
  *****************************************************************/
void Robot_Type(int robot_type)
{
    if(robot_type > 0)
    {
        if(robot_type == 1)
        {
            Robot.Robot_Type = LITE_DOG;
        }
    }
}

void Robot_Init(void)
{
  Lite_Dog_Init();
}

/******************************************************************
  * @ 函数名  ： Robot_Task
  * @ 功能说明： 机器人任务函数
  * @ 参数    ： NULL 
  * @ 返回值  ： NULL
  *****************************************************************/
Robot_Type_Struct Robot_Type_Last;
void Robot_Task(void)
{
  Robot_Init();
  while(1)
  {
    if(Robot.Robot_Type != ROBOT_NONE)
    {
      if(Robot.Robot_Type == LITE_DOG)
      {
        if(Robot.Robot_Type != Robot_Type_Last)
        {
          Lite_Dog_Init();
        }
        Lite_Dog_Task();
      }
      else
      {
        osDelay(1);
      }
    }
    else
    {
      osDelay(1);
    }
    Robot_Type_Last = Robot.Robot_Type;
  }
}

/******************************************************************
  * @ 函数名  ： Robot_Usb_Callback
  * @ 功能说明： 串口回调函数
  * @ 参数    ： NULL
  * @ 返回值  ： NULL
  *****************************************************************/
void Robot_Usb_Callback(char *type, int robot_type)
{
	if(memcmp(type, "robot-type", 10) == 0)
	{
        Robot_Type(robot_type);
	}
}
