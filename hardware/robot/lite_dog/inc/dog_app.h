#ifndef DOG_APP_H
#define DOG_APP_H

#include "modular.h"
#include <delay.h>
#include <app.h>


#define Servo_Angle_Change_Data_set 1/4096*360//1/1024*300
#define Servo_Data_Change_Angle_set 1/360*4096//1/300*1024
#define Servo_B_Motor_Data_Angle_Change 1/23.5*15

void Get_XY_Coordinate(float Leg_A_Angle,float Leg_B_Angle,float* Get_X_Codi,float* Get_Y_Codi);

void Lite_Dog_Angle_To_Coordinate(Lite_Dog_Struct* All_Legs_Angle_Data);

void Get_Dog_Legs_Angle_XY(float Set_X_Data,float Set_Y_Data,float* Get_A_Angle_t,float* Get_B_Angle_t);

void Get_Dog_Legs_Angle_XYZ(Leg_Loc_Enum Leg_Loc_Set,float X_Data,float Y_Data,float Z_Data,float* A_Angle,float* B_Angle,float* C_Angle);

void Lite_Dog_Coordinate_To_Angle(Lite_Dog_Struct* Dog_Coordinate_To_Angle,float* X,float* Y,float* Z);

void Lite_Dog_Data_To_Servo(Lite_Dog_Struct* Dog_Data_To_Servo, uint16_t Time, uint16_t Speed);

void Lite_Dog_Attitude_To_Coordinate(Lite_Dog_Struct *Dog_Attitude_To_Coordinate, float* X, float* Y, float* Z);

void Lite_Dog_Attitude_Update(Dog_State_Data_Struct *Dog_Attitude_Update);

void Dog_Recovery_Event(Lite_Dog_Struct* Dog_Recovery);

void Lite_Dog_Set_Coordinate(Lite_Dog_Struct* Dog_Coordinate_To_Angle,float* X,float* Y,float* Z);

void Lite_Dog_Get_Coordinate(Lite_Dog_Struct* Dog_Coordinate_To_Angle,float* X,float* Y,float* Z);

void Lite_Dog_Set_PowerLess(void);

void Single_Leg_Set_PowerLess(Dog_Leg_Data_Struct* Leg_Set_PowerLess,uint8_t IFload);

// 获取参数
void Lite_Dog_Servo_To_Data(Lite_Dog_Struct* Dog_Servo_To_Leg);

// 单腿设置
void Single_Leg_Servo_Data_Check(Leg_Loc_Enum Leg_Loc_Set,int* Get_Servo_Set_Data,int* Get_Servo_OffSet_Data);

void Single_Leg_Coordinate_To_Angle(Dog_Leg_Data_Struct* Dog_Leg_Data_Set,float Set_X,float Set_Y,float Set_Z);

void Single_Leg_Angle_To_Cood(Dog_Leg_Data_Struct* Dog_Leg_Data_Set,float Set_A_Angle,float Set_B_Angle,float Set_C_Angle);

void Single_Leg_Data_To_Servo(Dog_Leg_Data_Struct* Single_Legs_Data,uint16_t Set_Time,uint16_t Set_Speed);

void Single_Leg_Get_Servo_Data(Dog_Leg_Data_Struct* Dog_Leg_Data_Get);

int Lite_Dog_Write_Flash_Data(Lite_Dog_Struct* Dog_Write_Data);

int Lite_Dog_Read_Flash_Data(Lite_Dog_Struct* Dog_Read_Data);


#endif



