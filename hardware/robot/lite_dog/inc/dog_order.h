#ifndef DOG_ORDER_H
#define DOG_ORDER_H

#include <delay.h>
#include <app.h>

#include "modular.h"
#include "lite_dog.h"


//指令初始化模式
#define Dog_Order_Init_Height 60.00f
#define Dog_Order_Init_Gravity 0.00f
#define Dog_Order_Init_Sport_Mode Trot_M
#define Dog_Order_Init_Set_Mode Dog_Order_Move_Mode
#define Dog_Order_Init_Move_Mode Stop_M
#define Dog_Order_Init_Roll 0.00f
#define Dog_Order_Init_Pitch 0.00f
#define Dog_Order_Init_Yaw 0.00f
#define Dog_Order_Init_Move_Gait 1000.00f
#define Dog_Order_Init_Move_Height 20.00f
#define Dog_Order_Init_Move_Lenth 20.00f



void Dog_Order_Mode_Init(void);

void Dog_Order_Motion_Data_Update(Lite_Dog_Struct* Dog_Order_MMU);

//狗狗模式回调函数
void Dog_Mode_Serial_Callback(char *mode);
//姿态控制回调函数
void Dog_Attitude_Serial_Callback(float* XYZ,float roll,float pitch,float yaw);
//运动控制回调函数
void Dog_Control_Serial_Callback(int Cycle, char *mode, char *control);
//全向运动控制回调函数
void Dog_Omni_Control_Serial_Callback(int Cycle, char *mode ,float Angle);
//腿部校准回调函数
void Dog_Celebration_Serial_Callback(char *leg, char *State);
//XY控制回调函数
void Dog_XYZ_Set_Serial_Callback(char *leg,int x,int y,int z,int speed,int time);
//狗狗自动模式切换回调函数
void Dog_Auto_Mode_Serial_Callback(char *Auto_Mode);

//设置步态周期
void Lite_Dog_Auto_Control_Set(float Set_Gait);
//设置步态模式
void Lite_Dog_Motion_Mode_Set(Dog_Sport_Mode_Enum Dog_Sport_Mode_Set);
//运动模式修改
void Lite_Dog_Motion_Control_Set(Dog_Move_Mode_Enum Dog_Move_Mode_Set);
//自动运动模式修改
void Lite_Dog_Auto_Control_Mode_Set(Dog_Order_Auto_Mode_Struct Dog_Move_Auto_Mode_Set);

//设置Pitch轴俯仰角度
void Lite_Dog_Write_Pitch(float Dog_Pitch_Set);
//设置Roll轴滚转角度
void Lite_Dog_Write_Roll(float Dog_Roll_Set);

void Lite_Dog_Write_Yaw(float Dog_Yaw_Set);

void Lite_Dog_Write_XYZ(float* XYZ_Data);

// //俯卧撑
// void Lite_Dog_Action_PushUp(int Move_Time,Lite_Dog_Struct* Dog_PushUp);
// //握个手，打招呼
// void Lite_Dog_Action_SayHello(int Move_Time,int Hand_Set,Lite_Dog_Struct* Dog_SayHello);
// //扭一扭，舞蹈
// void Lite_Dog_Action_Dance(int Move_Time,int Turn_Set,Lite_Dog_Struct* Dog_Dance);
// //随地大小便
// void Lite_Dog_Action_Pee(int Move_Time,int Leg_Set,Lite_Dog_Struct* Dog_Pee);

//设置校准相关参数
void Lite_Dog_Celebrate_Data_Set(Leg_Loc_Enum Set_Leg,Leg_Celebrate_State_Enum Set_State);

int Get_Dog_Clock(int Set_Time,int Reset_Flag);

void Lite_Dog_Action_Dance(Lite_Dog_Struct* Lite_Dog_Dance,int Action_Time);

//返回指令相关数据结构体地址
Dog_Order_Mode_Struct* Get_Order_Mode_Address(void);

#endif
