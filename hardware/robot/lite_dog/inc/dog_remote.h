#ifndef DOG_REMOTE_H
#define DOG_REMOTE_H

#include <delay.h>
#include <app.h>

#include "modular.h"
#include "lite_dog.h"

void Dog_Remote_Motion_Data_Get(Dog_Control_Data_Struct* Move_Data_Get,Dog_Move_Mode_Enum* Move_Mode_Set,float* Move_Height_Get,float* Move_Lenth_Get,float* Move_Speed_Get);

void Dog_RM_Dyna_Motion_Mode_Set(Lite_Dog_Struct* Dog_RM_DMM);

void Dog_RM_Adaptive_Angle_Mode_Set(Lite_Dog_Struct* Dog_RM_AAM);

void Dog_RM_Att_Control_Mode_Set(Lite_Dog_Struct* Dog_RM_AAM);

void Dog_Remote_Control_Mode_Set(Dog_Control_Data_Struct* Dog_Control_Data);

void Dog_Remote_Control_Mode_Update(Lite_Dog_Struct* Dog_Control_Data_Update);



#endif