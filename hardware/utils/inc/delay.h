#ifndef UTILS_DELAY
#define UTILS_DELAY

#include "los_tick.h"
#include "los_task.h"
#include "cmsis_os2.h"

void Delay_Init(uint32_t TICK_RATE_HZ);

void delay_us(unsigned int microsecond);

void delay_ms(unsigned int millisecond);

int Get_Tick_Count(unsigned long *count);

#endif
