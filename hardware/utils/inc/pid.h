#ifndef UTILS_PID
#define UTILS_PID

typedef struct{
	float kp,kd,ki;
	float error;
	float last_error;
	float output;
	float diff;
	float integral;
} Pid_Struct;

void Pid_Init(Pid_Struct *pid, float kp, float ki, float kd);

float Pid_Dog_Calculation(float target, float current);

#endif
