/**
  ******************************************************************************
  * @file    delay.c
  * @author  GEEKROS,  site:www.geekros.com
  ******************************************************************************
*/

#include "delay.h"

static uint8_t fac_us = 0;
static uint32_t fac_ms = 0;

/******************************************************************
  * @ 函数名  ： Delay_Init
  * @ 功能说明： 外部时钟初始化函数
  * @ 参数    ： NULL
  * @ 返回值  ： NULL
  *****************************************************************/
void Delay_Init(uint32_t TICK_RATE_HZ){
	uint32_t reload = 0;
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
	fac_us = SystemCoreClock / 8000000;
	fac_ms = SystemCoreClock / 8000;

	if (TICK_RATE_HZ == 0)
	{
		TICK_RATE_HZ = 1000;
	}

	reload = SystemCoreClock / TICK_RATE_HZ / 8;
	reload--;

	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;
	SysTick->LOAD = reload;
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
}

/******************************************************************
  * @ 函数名  ： delay_us
  * @ 功能说明： 微秒延时函数
  * @ 参数    ： unsigned int microsecond 延迟时间，单位微秒
  * @ 返回值  ： NULL
  *****************************************************************/
void delay_us(unsigned int microsecond)
{
	uint32_t ticks = 0;
	uint32_t told = 0;
	uint32_t tnow = 0;
	uint32_t tcnt = 0;
	uint32_t reload = 0;
	reload = SysTick->LOAD;
	ticks = microsecond * fac_us;
	told = SysTick->VAL;
	while (1)
	{
		tnow = SysTick->VAL;
		if (tnow != told)
		{
			if (tnow < told)
			{
				tcnt += told - tnow;
			}else{
				tcnt += reload - tnow + told;
			}
			told = tnow;
			if (tcnt >= ticks)
			{
				break;
			}
		}
	}
}

/******************************************************************
  * @ 函数名  ： delay_ms
  * @ 功能说明： 毫秒延时函数
  * @ 参数    ： unsigned int millisecond 延迟时间，单位毫秒
  * @ 返回值  ： NULL
  *****************************************************************/
void delay_ms(unsigned int millisecond)
{
	uint32_t ticks = 0;
	uint32_t told = 0;
	uint32_t tnow = 0;
	uint32_t tcnt = 0;
	uint32_t reload = 0;
	reload = SysTick->LOAD;
	ticks = millisecond * fac_ms;
	told = SysTick->VAL;
	while (1)
	{
		tnow = SysTick->VAL;
		if (tnow != told)
		{
			if (tnow < told)
			{
				tcnt += told - tnow;
			}else{
				tcnt += reload - tnow + told;
			}
			told = tnow;
			if (tcnt >= ticks)
			{
				break;
			}
		}
	}
}

/******************************************************************
  * @ 函数名  ： Get_Tick_Count
  * @ 功能说明： 未知
  * @ 参数    ： unsigned long *count
  * @ 返回值  ： NULL
  *****************************************************************/
int Get_Tick_Count(unsigned long *count)
{
	count[0] = LOS_SysCycleGet();
	return 0;
}
