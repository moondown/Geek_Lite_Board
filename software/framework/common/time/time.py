import time
import datetime
import threading

class Time:

    def __init__(self, framework):
        self._framework = framework
        self.time = time
        self.state = "end"
        self.millisecond = 0
        self.date_data = 0
        self.time_task = threading.Thread(name="start_task", target=self.start_task)
        self.time_task.daemon = True
        self.time_task.start()

    def start_task(self):
        while True:
            if self.state == "start":
                self.millisecond = self.millisecond + 1
            if self.state == "pause":
                self.millisecond = self.millisecond
            if self.state == "end":
                self.millisecond = 0

    def get(self):
        return self.millisecond

    def set_state(self, state):
        self.state = state

    def date(self, date):
        self.date_data = 0
        if date == "year":
            self.date_data = datetime.datetime.now().year
        if date == "month":
            self.date_data = datetime.datetime.now().month
        if date == "day":
            self.date_data = datetime.datetime.now().day
        if date == "hour":
            self.date_data = datetime.datetime.now().hour
        if date == "minute":
            self.date_data = datetime.datetime.now().minute
        if date == "second":
            self.date_data = datetime.datetime.now().second
        return self.date_data

    def sleep(self, second):
        self.time.sleep(second)
