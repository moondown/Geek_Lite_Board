from .power import Power
from .adc import Adc
from .cpu import Cpu
from .led import Led
from .buzzer import Buzzer
from .io import Io
from .key import Key
from .pwm import Pwm
from .can import Can
from .rocker import Rocker
from .mpu import Mpu
from .card import Card
from .hmi import HMI
from .wifi import WIFI
from .servo import Servo
import app as robot_script

class Board:

    def __init__(self, framework):
        self._framework = framework
        self.power = Power(framework)
        self.adc = Adc(framework)
        self.cpu = Cpu(framework)
        self.led = Led(framework)
        self.io = Io(framework)
        self.key = Key(framework)
        self.buzzer = Buzzer(framework)
        self.pwm = Pwm(framework)
        self.can = Can(framework)
        self.rocker = Rocker(framework)
        self.mpu = Mpu(framework)
        self.hmi = HMI(framework)
        self.wifi = WIFI(framework)
        self.servo = Servo(framework)
        self.card = Card(framework)
        self.func = dir(robot_script)

    def message_callback(self, message):
        # 开发板电压数据
        if message["type"] == "adc-board-voltage":
            self.adc.voltage = message["voltage"]
        
        # 开发板PWM电压数据
        if message["type"] == "adc-pwm-voltage":
            self.adc.pwm_voltage = message["voltage"]
        
        # 开发板CPU温度数据
        if message["type"] == "cpu-temperature":
            self.cpu.temperature = message["temperature"]
        
        # 开发板MPU数据
        if message["type"] == "mpu-read":
            self.mpu.pit = message["pit"]
            self.mpu.yaw = message["yaw"]
            self.mpu.rol = message["rol"]
            self.mpu.temperature = message["temperature"]
            self.mpu.pressure = message["pressure"]
            self.mpu.altitude = message["altitude"]
        
        # 开发板自定义按键状态数据
        if message["type"] == "key-state":
            self.key.key_state = message["state"]
            func_str = "key_state_{}_event".format(message["state"])
            if func_str in self.func:
                getattr(robot_script, func_str)(self._framework)
        
        # 开发板IO模式状态数据
        if message["type"] == "io-mode":
            func_str = "io_mode_channel_{}_data_{}_event".format(message["channel"], message["mode"])
            if func_str in self.func:
                getattr(robot_script, func_str)(self._framework)
        
        # 开发板IO高低电平状态数据
        if message["type"] == "io-state":
            func_str = "io_state_channel_{}_data_{}_event".format(message["channel"], message["state"])
            if func_str in self.func:
                getattr(robot_script, func_str)(self._framework)
        
        # 开发板遥控器数据
        if message["type"] == "rocker-control-data":
            self.rocker.ch0 = message["ch0"]
            self.rocker.ch1 = message["ch1"]
            self.rocker.ch2 = message["ch2"]
            self.rocker.ch3 = message["ch3"]
            self.rocker.ch4 = message["ch4"]
            self.rocker.s1 = message["s1"]
            self.rocker.s2 = message["s2"]
