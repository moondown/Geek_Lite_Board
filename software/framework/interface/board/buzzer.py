class Buzzer:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def tone(self, tone):
        self.command = {"type": "buzzer-tone", "tone": tone}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def stop(self):
        self.command = {"type": "buzzer-stop"}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
