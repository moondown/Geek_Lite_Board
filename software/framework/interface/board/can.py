class Can:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def can_motor_sid(self, can_channel, can_motor, motor_read, motor_write):
        self.command = {"type": "can-motor-sid", "channel": can_channel, "motor": can_motor, "read": motor_read, "write": motor_write}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def can_motor_current(self, can_channel, motor_id, motor_current):
        self.command = {"type": "can-motor-current", "channel": can_channel, "motor": motor_id, "current": motor_current}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def can_motor_multiple_current(self, can_channel, motor_multiple, motor_current):
        self.command = {"type": "can-motor-multiple-current", "channel": can_channel, "multiple": motor_multiple, "current": motor_current}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
