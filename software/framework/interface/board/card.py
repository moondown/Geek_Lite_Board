class Card:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}
        self.content = ""

    def card_file(self, file_name, file_content):
        self.command = {"type": "card-file", "name": file_name, "content": file_content}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def card_append_content(self, file_name, file_content):
        self.command = {"type": "card-append-content", "name": file_name, "content": file_content}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def card_delete(self, file_name):
        self.command = {"type": "card-delete", "name": file_name}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def card_read_content(self, file_name):
        self.command = {"type": "card-read-content", "name": file_name}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
