class HMI:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def set_rate(self, rate):
        self.command = {"type": "hmi-rate", "rate": rate}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
