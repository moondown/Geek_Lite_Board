class Io:

    def __init__(self, framework):
        self._framework = framework
        self.command = {}

    def set_mode(self, channel, mode):
        self.command = {"type": "io-mode", "channel": channel, "mode": mode}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)

    def set_state(self, channel, state):
        self.command = {"type": "io-state", "channel": channel, "state": state}
        if self._framework.utils.device.device_connect:
            self._framework.utils.device.device_write(self.command)
