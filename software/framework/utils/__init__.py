from .device.device import Device
from .display.display import Display
from .message.message import Message

class Utils:

    def __init__(self, framework):
        self.device = Device(framework)
        self.display = Display(framework)
        self.message = Message(framework)