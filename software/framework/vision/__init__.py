from .body.body import Body
from .gesture.gesture import Gesture

class Vision:

    def __init__(self, framework):
        self.body = Body(framework)
        self.gesture = Gesture(framework)